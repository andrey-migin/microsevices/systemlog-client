using System;

namespace SystemLog
{
    public interface ISystemLogWriter
    {
        void WriteInfo(string process, string message);
        void WriteWarning(string process, string message);
        void WriteError(string process, Exception exception);
    }
}