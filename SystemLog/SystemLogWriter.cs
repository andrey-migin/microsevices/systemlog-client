﻿using System;
using System.Collections.Generic;
using SystemLog.Grpc;
using SystemLog.Grpc.Contracts;
using SystemLog.Grpc.Models;
using DotNetCoreDecorators;

namespace SystemLog
{
    public class SystemLogWriter : ISystemLogWriter
    {
        private readonly ISystemLogService _systemLogService;
        private readonly string _component;

        public SystemLogWriter(ISystemLogService systemLogService, string componentName)
        {
            _systemLogService = systemLogService;
            _component = componentName;
        }
        
        
        private List<LogEventModel> _events 
            = new List<LogEventModel>();
        
        private readonly object _lockObject = new object();

        private DateTime _lastDateTime = DateTime.UtcNow.AddDays(-1);

        private void EnqueueEvent(LogEventModel logEvent)
        {
            lock (_lockObject)
            {
                Start();
                
                while (true)
                {
                    if (logEvent.DateTime != _lastDateTime)
                    {
                        _events.Add(logEvent);
                        _lastDateTime = logEvent.DateTime;
                        return;
                    }

                    logEvent.DateTime = logEvent.DateTime.AddMilliseconds(1);
                }
            }

        }

        public void WriteInfo(string process, string message)
        {
            EnqueueEvent(new LogEventModel
            {
                DateTime = DateTime.UtcNow,
                Message = message,
                Process = process,
                LogLevel = LogLevel.Info,
            });
        }

        public void WriteWarning(string process, string message)
        {
            EnqueueEvent(new LogEventModel
            {
                DateTime = DateTime.UtcNow,
                Message = message,
                Process = process,
                LogLevel = LogLevel.Warning,
            });
        }
        
        public void WriteError(string process, Exception exception)
        {
            EnqueueEvent(new LogEventModel
            {
                DateTime = DateTime.UtcNow,
                Message = exception.Message,
                Process = process,
                StackTrace = exception.StackTrace,
                LogLevel = LogLevel.Error,
            });
        }

        
        private readonly TaskTimer _threadTimer = new TaskTimer(1000);

        private IReadOnlyList<LogEventModel> GetEventsToPush()
        {

            lock (_lockObject)
            {
                if (_events.Count == 0)
                    return null;

                var result = _events;
                _events = new List<LogEventModel>();
                return result;
            }

        }

        private bool _isStarted;

        private void Start()
        {

            if (_isStarted)
                return;

            _isStarted = true;

            _threadTimer.Register("SystemLog Pusher", async () =>
            {
                var eventsToPush = GetEventsToPush();

                if (eventsToPush == null)
                    return;

                await _systemLogService.RegisterAsync(new LogEventRequest
                {
                    Component = _component,
                    Events = eventsToPush
                });

            });

            _threadTimer.Start();

        }

    }
}